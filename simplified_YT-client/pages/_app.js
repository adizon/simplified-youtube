import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import Head from 'next/head'
import {Container} from 'react-bootstrap'

function MyApp({ Component, pageProps }) {
  return (
  <>
    <Head>
      <title>Simplified Youtube</title>
    </Head>
    <Container>
      <Component {...pageProps} />
    </Container>
  </>
  )
}

export default MyApp
