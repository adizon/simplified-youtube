
import {Container, Row, Col, Jumbotron, Card, Button, Form} from 'react-bootstrap'
import {useEffect, useState} from 'react'

export default function Home() {
  const [files, setFiles] = useState([])
  const [isOpen, setIsOpen] = useState(false)

  useEffect(()=>{
    fetch('http://localhost:4000/upload')
      .then(res => res.json())
      .then(data => {
        setFiles(data.data)
      })
  })

  const uploadVideo = (file) => {
    const fd = new FormData();
    fd.append('video', file);

    fetch('http://localhost:4000/upload', {
      method: 'POST',
      
      body: fd
    })
      .then(res => res.json())
      .then(data => {
        console.log(data)
      })

  }

  return (
    <Container>
        <Jumbotron>
          <h1>Welcome to Simplified Youtube</h1>
        </Jumbotron>

      <Button class="my-5" variant="secondary" onClick={()=>setIsOpen(!isOpen)}>Upload a video</Button>
      {
        isOpen
          ? 
          <Form>
            <Form.Group>
              <Form.File id="videofile" label="Video to Upload: " name="video"/>
              <Button variant="primary" onClick={(e) => uploadVideo(video)}>Upload</Button>
            </Form.Group>
          </Form>
        : null
      }

      <br/>
      <Row className="my-3">
        {
        files.map(file => {
          return (
            <Col xs={12} lg={3}>
              <Card>
                <Card.Body>
                  <Card.Title>{file}
                  </Card.Title>
                </Card.Body>
              </Card>
            </Col>
          )
        })
      }
      </Row>



    </Container>
  )
}
