const express = require('express');
const fileUpload = require('express-fileupload');
const cors = require('cors');
const dotenv = require('dotenv').config();

const app = express();
const port = process.env.PORT;

// enable files upload
app.use(fileUpload({
    createParentPath: true
}));

let corsOptions = {
    origin: 'http://localhost:3000',
    optionsSuccessStatus: 200   // default
}


app.use(cors(corsOptions))

app.listen(port, () => {
    console.log("API is now online. Listening on port " + port)
})

app.post('/upload', async (req, res) => {
    try {
        if (!req.files) {
            res.send({
                status: false,
                message: 'No file uploaded'
            });
        } else {
            //Use the name of the input field (i.e. "avatar") to retrieve the uploaded file
            let video = req.files.video;

            //Use the mv() method to place the file in upload directory (i.e. "uploads")
            video.mv('./uploads/' + video.name);

            //send response
            res.send({
                status: true,
                message: 'File is uploaded',
                data: {
                    name: video.name,
                    mimetype: video.mimetype,
                    size: video.size
                }
            });
        }
    } catch (err) {
        res.status(500).send(err);
    }
});

app.get('/upload', (req, res) => {
    let fs = require('fs');
    let files = fs.readdirSync('./uploads');

    res.send({
        status: true,
        message: 'Files fetched successfully',
        data: files
    });
});
